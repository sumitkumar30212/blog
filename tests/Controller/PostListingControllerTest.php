<?php

namespace App\Tests\Controller;

use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;



class PostListingControllerTest extends  WebTestCase
{
    /**
     * test the create method
     */
    public function testcreate()
    {

        $client = static::createClient();
        $this->postData = array(
            'title' => 'php unit',
            'content' => 'just finding the curse',
        );
        $client = $this->createAuthenticatedClient();
        $response = $client->request(
            'POST',
            '/api/post_listings',
            array('body' => json_encode($this->postData)),
            array(),
            array('CONTENT_TYPE' => 'application/json')
        );
        $data = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals(
            200, // or Symfony\Component\HttpFoundation\Response::HTTP_OK
            $client->getResponse()->getStatusCode()
        );
    }
    /**
     * Get the existing posts
     */
    public function testpostlisting(){
        $client = $this->createAuthenticatedClient();
        $response = $client->request(
            'GET',
            '/api/post_listings'
        );
        $data = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals(
            200, // or Symfony\Component\HttpFoundation\Response::HTTP_OK
            $client->getResponse()->getStatusCode()
        );
    }
    /**
     * Create a client with a default Authorization header.
     *
     * @param string $username
     * @param string $password
     *
     * @return \Symfony\Bundle\FrameworkBundle\Client
     */
    protected function createAuthenticatedClient($username = 'sumitkumar100686@gmail.com', $password = 'admin123')
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/api/login',
            array(
                'email' => $username,
                'password' => $password,
            ),
            array(),
            array('CONTENT_TYPE' => 'application/json')
        );

        $data = json_decode($client->getResponse()->getContent(), true);
        $client = static::createClient();
        $client->setServerParameter('HTTP_Authorization', sprintf('Bearer %s', $data['token']));

        return $client;
    }
}
