<?php

namespace App\Repository;

use App\Entity\BloggingRequest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method BloggingRequest|null find($id, $lockMode = null, $lockVersion = null)
 * @method BloggingRequest|null findOneBy(array $criteria, array $orderBy = null)
 * @method BloggingRequest[]    findAll()
 * @method BloggingRequest[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BloggingRequestRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BloggingRequest::class);
    }

    // /**
    //  * @return BloggingRequest[] Returns an array of BloggingRequest objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BloggingRequest
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
