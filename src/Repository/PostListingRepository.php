<?php

namespace App\Repository;

use App\Entity\PostListing;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PostListing|null find($id, $lockMode = null, $lockVersion = null)
 * @method PostListing|null findOneBy(array $criteria, array $orderBy = null)
 * @method PostListing[]    findAll()
 * @method PostListing[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostListingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PostListing::class);
    }

    // /**
    //  * @return PostListing[] Returns an array of PostListing objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PostListing
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
