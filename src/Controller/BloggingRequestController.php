<?php

namespace App\Controller;


use  EasyCorp\Bundle\EasyAdminBundle\Controller\AdminController;

use App\Entity\BloggingRequest;
use App\Entity\User;

class BloggingRequestController extends AdminController
{
    /**
     * @param BloggingRequest $entity
       
     */
    protected function preUpdateEntity($entity)
    {
        $id = $this->request->query->get('id');
        $status = $_POST['bloggingrequest']['status'];
        $item =  $this->getDoctrine()
            ->getRepository(BloggingRequest::class)
            ->findOneBy(['id' => $id]);
        $userDetails = $this->getDoctrine()->getRepository(User::class);

        $userInfo = $userDetails->findOneBy([
            'id' => $item->getUser()
        ]);
        $userEmail = $userInfo->getEmail();
        if(strtolower($status)=='declined'){
            $text = "declined";
        }else{
            $text = "approved";
        }
        if($userInfo->getEmail() !=''){
            $this->sendEmail($userInfo->getEmail(),$text);
        }
        return $this->redirectToRoute('easyadmin', [
            'action' => 'show',
            'entity' => $this->request->query->get('entity'),
            'id' => $id,
        ]);
    }
    /**
     * Sending email after request process
     * 
     **/
    public function sendEmail($mailTo,$text){
        $message = (new \Swift_Message('Hello Email'))
        ->setSubject('Hello Email')
        ->setFrom("sumitkumar100686@gmail.com")
        ->setTo('sumitkumar100686@gmail.com')
        ->setBody(
            "Your request for blogging has been ".$text 
        );
        return true;
    }
}
