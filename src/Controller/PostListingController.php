<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\PostListing;

/**
 * Used to add/edit the new post
 */
class PostListingController extends Controller
{

    /**
     * @Route("/api/post_listings")
     * @Method("GET")
     */
    public function postlisting()
    {
        $userId = $this->get('security.token_storage')->getToken();
        $postDetails = $this->getDoctrine()->getRepository(PostListing::class);

        $postListings = $postDetails->findAll();
        $data = array('PostListing' => array());
        foreach ($postListings as $postListing) {
            $data['PostListing'][] = $this->serializePostListing($postListing);
        }
        $response = new JsonResponse($data, 200);
        return $response;
    }
    /**
     * This will be responsible for create Post for valid approved blogger
     */
    public function create(Request $request)
    {
        try {
            $token = $this->get('security.token_storage')->getToken();
            $user =  $token->getUser();
            $userDetails = $this->getDoctrine()
                ->getRepository(\App\Entity\BloggingRequest::class)
                ->findOneBy(['user' => $user->getId()]);
            if (isset($userDetails)) {
                if ($userDetails->getStatus() != 'approved') {
                    $status = 1;
                    $message = 'Your are not authorize to add new post.';
                } else {
                    $postRequest = new PostListing();
                    $postRequest->setTitle($request->request->get('title'));
                    $postRequest->setContent($request->request->get('content'));
                    $postRequest->setOwner($user);
                    $manager = $this->getDoctrine()->getManager();
                    $manager->persist($postRequest);
                    $manager->flush();
                    $status = 1;
                    $message = 'Post sucessfully created';
                }
            } else {
                $status = 1;
                $message = 'Your are not authorize to add new post.';
            }
            //'Your are not authorize to add new post.'
            $array = array(
                'status' => $status,
                'message' => $message
            );
            $response = new JsonResponse($array, 200);
            return $response;
        } catch (Exception $e) {
            $array = array('status' => 0, 'message' => 'Something went wrong');
            $response = new JsonResponse($array, 400);
            return $response;
        }
    }
    /**
     * Existing posts will be edited by this code block
     * @Route("/api/post_listings/{id}")
     * @Method("PUT")
     */
    public function edit($id, Request $request)
    {
        $post = $this->getDoctrine()
            ->getRepository(\App\Entity\PostListing::class)
            ->findOneBy(['id' => $id]);

        if (!$post) {
            $array = array('status' => 1, 'message' => 'Post does not exists');
        } else {
            $data = json_decode($request->getContent(), true);
            $post->setTitle($data['title']);
            $post->setContent($data['content']);
            $editPost = $this->getDoctrine()->getManager();
            $editPost->persist($post);
            $editPost->flush();
        }
        $array = array('status' => 1, 'message' => 'Post edit succesfully');
        $response = new JsonResponse($array, 400);
        return $response;
    }
    /**
     * Serialze data for json response
     */
    private function serializePostListing(PostListing $postListing)
    {
        return array(
            'id' => $postListing->getId(),
            'title' => $postListing->getTitle(),
            'content' => $postListing->getContent()
        );
    }
}
