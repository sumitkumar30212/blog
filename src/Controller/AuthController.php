<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Entity\BloggingRequest;

/**
 * Manage the auth mechanism
 */
class AuthController extends Controller
{
    /**
     * Login process handle
     */
    public function login(Request $request)
    {
        $user = $this->getDoctrine()
            ->getRepository(\App\Entity\User::class)
            ->findOneBy(['email' => $request->request->get('email')]);

        if (!$user) {
            throw $this->createNotFoundException();
        }

        $isValid = $this->get('security.password_encoder')
            ->isPasswordValid($user, $request->request->get('password'));

        if (!$isValid) {
            throw new BadCredentialsException();
        }

        $token = $this->get('lexik_jwt_authentication.encoder')
            ->encode([
                'username' => $user->getUsername(),
                'exp' => time() + 3600 // 1 hour expiration
            ]);

        return new JsonResponse(['token' => $token]);
    }
    /**
     * Used to register new user in system
     */
    public function register(Request $request)
    {
        $userManager = $this->get('fos_user.user_manager');
        $email = $request->request->get('email');
        $email_exist = $userManager->findUserByEmail($email);
        if ($email_exist) {
            return new JsonResponse(['status'=>'Fail','message' => 'Email Id already exists.']);
        }
        $user = $userManager->createUser();
        $user->setUsername($request->request->get('username'));
        $user->setEmail($email);
        $user->setEmailCanonical($email);
        $user->setEnabled(0);
        $user->setPlainPassword($request->request->get('password'));
        $user->setFirstName($request->request->get('firstname'));
        $user->setLastName($request->request->get('lastname'));
        $user->addRole('ROLE_USER');
        $userManager->updateUser($user);
        return new JsonResponse(['status'=>'Success','message' => 'You are successfully registered']);
    }
    /**
     * verification of existing users 
     */
    public function verification(Request $request)
    {
        try {
            $userId = $this->get('security.token_storage')->getToken();
            $bloggingRequest = new BloggingRequest();
            $file = $request->files->get('file');
            $fileName = md5(uniqid()) . '.' . $file->guessExtension();
            $original_name = $file->getClientOriginalName();
            $file->move($this->container->getParameter('user_image_directory'), $fileName);
            $bloggingRequest->setUser($userId->getUser());
            $bloggingRequest->setFilepath($original_name);
            $bloggingRequest->setStatus('Pending');
            $bloggingRequest->setDescription($request->request->get('description'));
            $createdAt = new \DateTimeImmutable();
            $updatedAt = new \DateTimeImmutable();
            $bloggingRequest->setCreatedAt($createdAt);
            $bloggingRequest->setUpdatedAt($updatedAt);
            $manager = $this->getDoctrine()->getManager();
            $manager->persist($bloggingRequest);
            $manager->flush();
            $array = array(
                'status' => 1
            );
            $response = new JsonResponse($array, 200);
            return $response;
        } catch (Exception $e) {
            $array = array('status' => 0);
            $response = new JsonResponse($array, 400);
            return $response;
        }
    }
}
