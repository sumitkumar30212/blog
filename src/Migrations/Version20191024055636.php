<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191024055636 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE post_listing ADD owner_id INT NOT NULL, DROP owner');
        $this->addSql('ALTER TABLE post_listing ADD CONSTRAINT FK_F6E8FFE77E3C61F9 FOREIGN KEY (owner_id) REFERENCES users (id)');
        $this->addSql('CREATE INDEX IDX_F6E8FFE77E3C61F9 ON post_listing (owner_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE post_listing DROP FOREIGN KEY FK_F6E8FFE77E3C61F9');
        $this->addSql('DROP INDEX IDX_F6E8FFE77E3C61F9 ON post_listing');
        $this->addSql('ALTER TABLE post_listing ADD owner VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, DROP owner_id');
    }
}
