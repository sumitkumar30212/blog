<?php
// src/AppBundle/Entity/User.php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 * @ApiResource(attributes={
 *     "normalization_context"={"groups"={"read"}},
 *     "denormalization_context"={"groups"={"write"}}
 * }))
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    protected $firstName;
    protected $lastName;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\BloggingRequest", mappedBy="user")
     */
    private $bloggingRequests;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PostListing", mappedBy="owner")
     */
    private $postListings;

    public function __construct()
    {
        parent::__construct();
        $this->bloggingRequests = new ArrayCollection();
        $this->postListings = new ArrayCollection();
        // your own logic
    }
    /**
     * Set firstName
     *
     * @param String $firstName
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }
    /**
     * Get firstName
     *
     * @return String
     */
    public function getFirstName()
    {
        return $this->firstName;
    }
    /**
     * Set lastName
     *
     * @param String $lastName
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }
     /**
     * Get lastName
     *
     * @return String
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @return Collection|BloggingRequest[]
     */
    public function getBloggingRequests(): Collection
    {
        return $this->bloggingRequests;
    }

    public function addBloggingRequest(BloggingRequest $bloggingRequest): self
    {
        if (!$this->bloggingRequests->contains($bloggingRequest)) {
            $this->bloggingRequests[] = $bloggingRequest;
            $bloggingRequest->setUser($this);
        }

        return $this;
    }

    public function removeBloggingRequest(BloggingRequest $bloggingRequest): self
    {
        if ($this->bloggingRequests->contains($bloggingRequest)) {
            $this->bloggingRequests->removeElement($bloggingRequest);
            // set the owning side to null (unless already changed)
            if ($bloggingRequest->getUser() === $this) {
                $bloggingRequest->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PostListing[]
     */
    public function getPostListings(): Collection
    {
        return $this->postListings;
    }

    public function addPostListing(PostListing $postListing): self
    {
        if (!$this->postListings->contains($postListing)) {
            $this->postListings[] = $postListing;
            $postListing->setOwner($this);
        }

        return $this;
    }

    public function removePostListing(PostListing $postListing): self
    {
        if ($this->postListings->contains($postListing)) {
            $this->postListings->removeElement($postListing);
            // set the owning side to null (unless already changed)
            if ($postListing->getOwner() === $this) {
                $postListing->setOwner(null);
            }
        }

        return $this;
    }
}
